package com.cgi.bootcamp.jg.usermanagement.api;

import java.time.Clock;
import java.time.LocalDateTime;

public class ErrorDto {

    private String message;
    private LocalDateTime timestamp;

    public ErrorDto() {
        timestamp = LocalDateTime.now(Clock.systemUTC());
    }

    public ErrorDto(String message) {
        timestamp = LocalDateTime.now(Clock.systemUTC());
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ErrorDto{" +
                "message='" + message + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}

package com.cgi.bootcamp.jg.usermanagement.persistence.repository;

import com.cgi.bootcamp.jg.usermanagement.persistence.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("SELECT DISTINCT r FROM Role r JOIN r.groups AS g JOIN g.persons AS p WHERE p.id = :id")
    List<Role> findByPersonId(@Param("id") Long id);
}

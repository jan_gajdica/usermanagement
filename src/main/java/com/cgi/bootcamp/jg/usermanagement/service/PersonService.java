package com.cgi.bootcamp.jg.usermanagement.service;

import com.cgi.bootcamp.jg.usermanagement.api.PersonCompleteDto;
import com.cgi.bootcamp.jg.usermanagement.api.PersonSimpleDto;
import com.cgi.bootcamp.jg.usermanagement.api.RoleDto;
import com.cgi.bootcamp.jg.usermanagement.persistence.entities.Person;
import com.cgi.bootcamp.jg.usermanagement.persistence.entities.Role;
import com.cgi.bootcamp.jg.usermanagement.persistence.repository.PersonRepository;
import com.cgi.bootcamp.jg.usermanagement.persistence.repository.RoleRepository;
import com.cgi.bootcamp.jg.usermanagement.service.exceptions.EntityNotFoundException;
import com.cgi.bootcamp.jg.usermanagement.service.mapping.BeanMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PersonService {

    private PersonRepository personRepository;
    private RoleRepository roleRepository;
    private BeanMapping beanMapping;

    @Autowired
    public PersonService(PersonRepository personRepository, RoleRepository roleRepository, BeanMapping beanMapping) {
        this.personRepository = personRepository;
        this.roleRepository = roleRepository;
        this.beanMapping = beanMapping;
    }

    public PersonSimpleDto findPersonById(Long id) {
        Person person = personRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("person.id: '" + id.toString() + "' not found"));
        return beanMapping.mapTo(person, PersonSimpleDto.class);
    }

    public PersonSimpleDto findPersonsByEmail(String email) {
        Person person = personRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("person.email: '" + email + "' not found"));
        return beanMapping.mapTo(person, PersonSimpleDto.class);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    public PersonCompleteDto findCompletePersonById(Long id) {
        Person person = personRepository.findCompleteById(id)
                .orElseThrow(() -> new EntityNotFoundException("person.id: '" + id.toString() + "' not found"));
        return beanMapping.mapTo(person, PersonCompleteDto.class);
    }

    public List<RoleDto> findRolesByPersonId(Long id) {
        List<Role> roleList = roleRepository.findByPersonId(id);
        return beanMapping.mapTo(roleList, RoleDto.class);
    }
}

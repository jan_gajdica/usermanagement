package com.cgi.bootcamp.jg.usermanagement.persistence.repository;

import com.cgi.bootcamp.jg.usermanagement.persistence.entities.Person;
import com.cgi.bootcamp.jg.usermanagement.persistence.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query("SELECT p FROM Person p WHERE p.id = :id")
    Optional<Person> findById(@Param("id") Long id);

    @Query("SELECT p FROM Person p WHERE p.email = :email")
    Optional<Person> findByEmail(@Param("email") String email);

    @Query("SELECT p FROM Person p LEFT JOIN FETCH p.address AS a LEFT JOIN FETCH p.groups AS g LEFT JOIN FETCH g.roles AS r WHERE p.id = :id")
    Optional<Person> findCompleteById(@Param("id") Long id);
}

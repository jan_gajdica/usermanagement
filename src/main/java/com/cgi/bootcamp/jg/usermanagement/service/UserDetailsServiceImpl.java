package com.cgi.bootcamp.jg.usermanagement.service;

import com.cgi.bootcamp.jg.usermanagement.persistence.entities.Person;
import com.cgi.bootcamp.jg.usermanagement.persistence.entities.Role;
import com.cgi.bootcamp.jg.usermanagement.persistence.repository.PersonRepository;
import com.cgi.bootcamp.jg.usermanagement.persistence.repository.RoleRepository;
import com.cgi.bootcamp.jg.usermanagement.security.MyUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("proprietaryUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private PersonRepository personRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserDetailsServiceImpl(PersonRepository personRepository, RoleRepository roleRepository) {
        this.personRepository = personRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Person person = personRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("Person.email: " + username + " not found"));
        List<Role> roleList = roleRepository.findByPersonId(person.getId());
        return new MyUserPrincipal(person, roleList);
    }
}

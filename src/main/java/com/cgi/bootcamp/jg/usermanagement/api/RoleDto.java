package com.cgi.bootcamp.jg.usermanagement.api;

public class RoleDto {

    private Long id;
    private String roleType;
    private String description;

    public RoleDto() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "RoleDto{" +
                "id=" + id +
                ", roleType='" + roleType + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}

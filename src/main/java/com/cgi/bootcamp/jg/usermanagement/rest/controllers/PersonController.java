package com.cgi.bootcamp.jg.usermanagement.rest.controllers;

import com.cgi.bootcamp.jg.usermanagement.api.PersonCompleteDto;
import com.cgi.bootcamp.jg.usermanagement.api.PersonSimpleDto;
import com.cgi.bootcamp.jg.usermanagement.api.RoleDto;
import com.cgi.bootcamp.jg.usermanagement.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/persons")
public class PersonController {

    private PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonSimpleDto> getPersonById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(personService.findPersonById(id), HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonSimpleDto> getPersonByEmail(@RequestParam("email") String email) {
        return new ResponseEntity<>(personService.findPersonsByEmail(email), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}/roles", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RoleDto>> getRolesByPersonId(@PathVariable("id") Long id) {
        List<RoleDto> roleList = personService.findRolesByPersonId(id);
        HttpStatus status = HttpStatus.OK;
        if (roleList.isEmpty()) status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(roleList, status);
    }

    @GetMapping(path = "/{id}/details", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonCompleteDto> getCompletePersonById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(personService.findCompletePersonById(id), HttpStatus.OK);
    }
}

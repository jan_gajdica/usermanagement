package com.cgi.bootcamp.jg.usermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class UsermanagementApplication {

    public static void main(String[] args) {

        ApplicationContext applicationContext = SpringApplication.run(UsermanagementApplication.class, args);
    }

}

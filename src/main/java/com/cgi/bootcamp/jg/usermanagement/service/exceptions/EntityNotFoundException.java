package com.cgi.bootcamp.jg.usermanagement.service.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message) {
        super(message);
    }
}
